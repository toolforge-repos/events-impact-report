
import pandas as pd
import numpy as np


def filter_by_wiki_project_name(filter_selection, filtered_events_df, filtered_contributions_df, mappings):
    if filter_selection != 'All':
        filtered_events_df = filtered_events_df[
            filtered_events_df['wiki_dbs_list'].fillna('').apply(
                lambda x: mappings['wiki_name2db_lookup'][filter_selection] in x
            )
        ]
        filtered_contributions_df = filtered_contributions_df[
            filtered_contributions_df['wiki_db'] == mappings['wiki_name2db_lookup'][filter_selection]
        ]
    return filtered_events_df, filtered_contributions_df

def filter_by_grant_funding_status(filter_selection, filtered_events_df, filtered_contributions_df):
    if filter_selection == 'Funded':
        filtered_events_df = filtered_events_df[
            filtered_events_df['grant_funded_event'] == True
        ]
        filtered_contributions_df = filtered_contributions_df[
            filtered_contributions_df['grant_funded_event'] == True
        ]
    elif filter_selection == 'Not funded':
        filtered_events_df = filtered_events_df[
            filtered_events_df['grant_funded_event'] == False
        ]
        filtered_contributions_df = filtered_contributions_df[
            filtered_contributions_df['grant_funded_event'] == False
        ]
    return filtered_events_df, filtered_contributions_df


def filter_by_grant_region(filter_selection, filtered_events_df, filtered_contributions_df):
    if filter_selection == 'N/A':
        filtered_events_df = filtered_events_df[
            filtered_events_df['grant_funded_event'] == False
        ]
        filtered_contributions_df = filtered_contributions_df[
            filtered_contributions_df['grant_funded_event'] == False
        ]
    elif filter_selection == 'Unknown':
        filtered_events_df = filtered_events_df[
            (filtered_events_df['grant_funded_event'] == True)
            & (filtered_events_df['grant_country_wikimedia_region'].isna())
        ]
        filtered_contributions_df = filtered_contributions_df[
            (filtered_contributions_df['grant_funded_event'] == True)
            & (filtered_contributions_df['grant_country_wikimedia_region'].isna())
        ]
    elif filter_selection != 'All':
        filtered_events_df = filtered_events_df[
            filtered_events_df['grant_country_wikimedia_region'] == filter_selection
        ]
        filtered_contributions_df = filtered_contributions_df[
            filtered_contributions_df['grant_country_wikimedia_region'] == filter_selection
        ]
    return filtered_events_df, filtered_contributions_df


def filter_by_grant_country(filter_selection, filtered_events_df, filtered_contributions_df):
    if filter_selection == 'N/A':
        filtered_events_df = filtered_events_df[
            filtered_events_df['grant_funded_event'] == False
        ]
        filtered_contributions_df = filtered_contributions_df[
            filtered_contributions_df['grant_funded_event'] == False
        ]
    elif filter_selection == 'Unknown':
        filtered_events_df = filtered_events_df[
            (filtered_events_df['grant_funded_event'] == True)
            & (filtered_events_df['grant_country_list_standardized'].isna())
        ]
        filtered_contributions_df = filtered_contributions_df[
            (filtered_contributions_df['grant_funded_event'] == True)
            & (filtered_contributions_df['grant_country_list_standardized'].isna())
        ]
    elif filter_selection != 'All':
        filtered_events_df = filtered_events_df[
            filtered_events_df['grant_country_list_standardized'] == filter_selection
        ]
        filtered_contributions_df = filtered_contributions_df[
            filtered_contributions_df['grant_country_list_standardized'] == filter_selection
        ]

    return filtered_events_df, filtered_contributions_df



def apply_filters(form, filtered_events_df, filtered_contributions_df, mappings):
    filters = {}
    if form.validate_on_submit():

        if 'wiki_project' in form:
            filtered_events_df, filtered_contributions_df = filter_by_wiki_project_name(
                form.wiki_project.data, 
                filtered_events_df, 
                filtered_contributions_df, 
                mappings
            )

        if 'grant_funding_status' in form:
            filtered_events_df, filtered_contributions_df = filter_by_grant_funding_status(
                form.grant_funding_status.data, 
                filtered_events_df, 
                filtered_contributions_df
            )

        if 'grant_region' in form:
            filtered_events_df, filtered_contributions_df = filter_by_grant_region(
                form.grant_region.data, 
                filtered_events_df, 
                filtered_contributions_df
            )

        if 'grant_country' in form:
            filtered_events_df, filtered_contributions_df = filter_by_grant_country(
                form.grant_country.data, 
                filtered_events_df, 
                filtered_contributions_df
            )

    return filtered_events_df, filtered_contributions_df


def selection_details(form, filtered_contributions_df, filtered_events_df):
    selection = dict(
        wiki_project = form.wiki_project.data,
        grant_funding_status = form.grant_funding_status.data,
        grant_region = form.grant_region.data,
        grant_country = form.grant_country.data,

        num_events = len(filtered_events_df),
        num_grants = filtered_events_df['grant_id'].nunique(),
        num_edits = filtered_contributions_df['unreverted_edits_count'].sum(),
        num_participants = filtered_events_df['participants_count'].fillna(0).astype(int).sum()
    )

    return selection

