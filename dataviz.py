import plotly.graph_objs as go
from plotly.subplots import make_subplots
import plotly.express as px

import pandas as pd
import numpy as np

from datetime import datetime, timedelta

import dataprep

import config


COLORS = {
    'background': '#eaecf0',
    'chart_area_highlight': '#00dfff', #'#49ff00', # '#69da3c', #'#fdff00',

    'events': '#0d6efd', # blue
    'grants': 'orange'
}

chart_height_buffer_size = 0.01



def make_3years_in_months_daterange(months_series):
    '''Makes a months date range for charts for the past 3 years relative to the processing month.'''

    processing_month = months_series.max()
    processing_year = processing_month[:4]
    processing_month_value = processing_month[5:7]
    start_month = f'{str(int(processing_year) - 3)}-{processing_month_value}'
    date_range = pd.DataFrame(
        index=pd.date_range(
        start=start_month, 
        end=processing_month,
        freq='MS')
    )
    date_range['processing_month'] = date_range.index.strftime('%Y-%m')
    
    return date_range


def base_time_series_chart_layout():
    ### base chart layout ######################################################

    ts_layout = go.Layout(
        xaxis=dict(
            #tickangle=45,
            ticks="outside",
            ticklen=4
        ),
        width=600,

        margin=go.layout.Margin(
            l=2, 
            r=2, 
            b=2, 
            t=30
        ),
        plot_bgcolor='white',
        paper_bgcolor=COLORS['background']
    )
    return ts_layout



def events_grants_chart(filtered_events_df):
    date_range = make_3years_in_months_daterange(filtered_events_df['processing_month'])

    filtered_events_ts = date_range.merge(
        filtered_events_df.groupby('processing_month')['event_id'].count(),
        how='left',
        on='processing_month'
    ).set_index('processing_month')['event_id'].fillna(0)

    filtered_grants_ts = date_range.merge(
        filtered_events_df[filtered_events_df['grant_id'].notna()]
            .groupby('grant_agreement_month')['grant_id'].nunique().reset_index(),
        how='left',
        left_on='processing_month',
        right_on='grant_agreement_month'
    ).set_index('processing_month')['grant_id'].fillna(0)

    
    events_trace = go.Scatter(
        x=filtered_events_ts.index, 
        y=filtered_events_ts.values,
        mode='lines', 
        name='events',
        # opacity=0.8, 
        # marker_color=COLORS['events']
    )
    grants_trace = go.Bar(
        x=filtered_grants_ts.index, 
        y=filtered_grants_ts.values,
        name='grants',
        # opacity=0.8,  
        # marker=dict(
        #     color=COLORS['grants'],
        # ),
    )
    
    custom_layout = go.Layout(
        yaxis2=dict(
            title='Number of events',
            showgrid=False,
            range = [0, max(filtered_events_ts.max() * (1 + chart_height_buffer_size), 10)]
        ),
        yaxis=dict(
            title='Number of grants issued',
            showgrid=False,
            range = [0, max(filtered_grants_ts.max() * (1 + chart_height_buffer_size), 10)]
        ),
        legend=dict(
            orientation="h",
            #y = -0.2, 
            # y = 1.11, 
            # x = 0.6
        ),
    )

    # Create figure with secondary y-axis
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    fig.add_trace(grants_trace, secondary_y=False)
    fig.add_trace(events_trace, secondary_y=True)

    fig.update_layout(hovermode='x unified')

    fig.update_layout(base_time_series_chart_layout())
    fig.update_layout(custom_layout)

    return fig.to_html(full_html=False)


def participant_activity_chart(filtered_contributions_df, filtered_events_df):
    date_range = make_3years_in_months_daterange(filtered_events_df['processing_month'])
    edits_ts = date_range.merge(
        filtered_contributions_df.groupby('processing_month')[dataprep.unreverted_edit_counts_columns].sum(),
        how='left',
        on='processing_month'
    ).set_index('processing_month')[dataprep.unreverted_edit_counts_columns].fillna(0)

    event_participants_ts = date_range.merge(
        filtered_events_df.groupby('processing_month')['participants_count'].sum(),
        how='left',
        on='processing_month'
    ).set_index('processing_month')['participants_count'].fillna(0)
    


    custom_layout = go.Layout(
        yaxis=dict(
            title='Edits by participants',
            showgrid=False,
            range = [0, max(edits_ts['unreverted_edits_count'].max() * (1 + chart_height_buffer_size), 10)]
        ),
        yaxis2=dict(
            title='Number of participants',
            showgrid=False,
            range = [0, max(event_participants_ts.max() * (1 + chart_height_buffer_size), 10)]
        ),
        legend=dict(
            orientation="h",
        ),

        barmode='stack'
    )


    # Create figure with secondary y-axis
    fig = make_subplots(specs=[[{"secondary_y": True}]])
    for x in dataprep.unreverted_edit_counts_columns[:-1]:
        trace = go.Bar(
            x=edits_ts.index, 
            y=edits_ts[x],
            name=x[20:-7].replace('_', '-') + ' edits',
        )
        fig.add_trace(trace, secondary_y=False)


    event_participants_trace = go.Scatter(
        x=event_participants_ts.index, 
        y=event_participants_ts.values,
        mode='lines', 
        name='participants',
    )
    fig.add_trace(event_participants_trace, secondary_y=True)
    fig.update_layout(hovermode='x unified')

    fig.update_layout(base_time_series_chart_layout())
    fig.update_layout(custom_layout)

    return fig.to_html(full_html=False)


def by_country_chart(filtered_events_df):

    by_country_df = filtered_events_df.groupby(
        ['grant_funded_event', 'grant_country_wikimedia_region', 'grant_country_list_standardized'],
        dropna=False
    )['participants_count'].sum().sort_values().reset_index()

    unfunded_event_participants_count = by_country_df[by_country_df['grant_funded_event'] == False]['participants_count'].sum()
    unfunded_event_participants_pct = unfunded_event_participants_count / filtered_events_df['participants_count'].sum()

    by_country_df = by_country_df[by_country_df['grant_funded_event'] == True]
    if len(by_country_df) > 0:
        by_country_df = by_country_df.fillna('Unknown')
        by_country_df['grant_country_list_standardized'] = np.where(
            by_country_df['grant_country_list_standardized'].str.lower().str.contains('international'), 
            'International', 
            by_country_df['grant_country_list_standardized']
        )
        by_country_df['grant_country_list_standardized'] = np.where(
            by_country_df['grant_country_list_standardized'].str.lower().str.contains('regional'), 
            'Regional', 
            by_country_df['grant_country_list_standardized']
        )

    
    by_country_traces = []
    regions = list(by_country_df['grant_country_wikimedia_region'].unique())
    for r in regions:
        by_country_subset = by_country_df[by_country_df['grant_country_wikimedia_region'] == r]
        trace = go.Bar(
            y=by_country_subset['grant_country_list_standardized'], 
            x=by_country_subset['participants_count'],
            name=r,
            opacity=0.8,  
            orientation='h'
        )
        by_country_traces.append(trace)


    custom_layout = go.Layout(
        xaxis=dict(title='Number of participants'),
        yaxis=dict(
            categoryorder='array', 
            categoryarray= by_country_df['grant_country_list_standardized']
        ),
        legend=dict(
            orientation="h",
            title_text='Regions',
            y = -0.2, 
        ),
    )

    fig = go.Figure(data=by_country_traces)
    fig.update_layout(base_time_series_chart_layout())
    fig.update_layout(custom_layout)

    return fig.to_html(full_html=False)



def agg_stats_table(filtered_contributions_df, filtered_events_df):
    filtered_events_df['processing_year'] = filtered_events_df['processing_month'].str[:4].astype(int)
    filtered_events_df['grant_agreement_year'] = filtered_events_df['grant_agreement_month'].str[:4]
    
    processing_year = int(filtered_events_df['processing_month'].max()[:4])
    start_year = processing_year - 3
    year_range = pd.DataFrame(
        [x for x in range(start_year, processing_year+1)], 
        columns=['processing_year']
    )

    stats_table_format_mapper =  {}

    annual_stats = year_range.merge(
        filtered_events_df.groupby('processing_year')['event_id'].count(),
        how='left',
        on='processing_year'
    ).set_index('processing_year')
    annual_stats.rename(columns={'event_id': 'events'}, inplace=True)
    stats_table_format_mapper['events'] = '{:,.0f}'

    annual_grants = filtered_events_df[filtered_events_df['grant_id'].notna()]\
        .groupby('grant_agreement_year')['grant_id'].nunique().reset_index()
    annual_grants['grant_agreement_year'] = annual_grants['grant_agreement_year'].astype(int)
    annual_grants.set_index('grant_agreement_year', inplace=True)
    annual_grants.rename(columns={'grant_id': 'grants issued'}, inplace=True)
    stats_table_format_mapper['grants issued'] = '{:,.0f}'

    annual_stats = annual_stats.join(
        annual_grants,
        how='left'
    )

    # add participants count
    annual_stats = annual_stats.join(
        filtered_events_df.groupby('processing_year')['participants_count'].sum(),
        how='left'
    )
    annual_stats.rename(columns={'participants_count': 'participants'}, inplace=True)
    stats_table_format_mapper['participants'] = '{:,.0f}'

    # add organizers count
    annual_stats = annual_stats.join(
        filtered_events_df.groupby('processing_year')['organizers_count'].sum(),
        how='left'
    )
    annual_stats.rename(columns={'organizers_count': 'organizers'}, inplace=True)
    stats_table_format_mapper['organizers'] = '{:,.0f}'

    annual_stats = annual_stats.merge(
        filtered_contributions_df.groupby('processing_year')['unreverted_edits_count'].sum(),
        how='left',
        on='processing_year'
    )
    annual_stats.rename(columns={'unreverted_edits_count': 'edits'}, inplace=True)
    stats_table_format_mapper['edits'] = '{:,.0f}'

    # YOY percentages
    yoy_stats = annual_stats.fillna(0).pct_change().fillna(0) #.iloc[:-1]
    yoy_stats.loc[processing_year] = np.nan

    annual_stats = annual_stats.fillna(0).join(
        yoy_stats,
        how='left',
        rsuffix=' YOY'
    )
    stats_table_format_mapper_keys_list = list(stats_table_format_mapper.keys())
    for k in stats_table_format_mapper_keys_list:
        stats_table_format_mapper[f'{k} YOY'] = '{:.0%}'

    annual_stats[~pd.isna(annual_stats)] = annual_stats.apply(lambda x: x.apply(stats_table_format_mapper[x.name].format))
    annual_stats = annual_stats.fillna('-').transpose().rename(columns={processing_year: f'{processing_year} YTD'})

    return annual_stats.to_html(index_names=False, classes='table')



def event_participants_chart(filtered_contributions_df, filtered_events_df):

    # date range for event charts
    # starts at event start and ends at processing month
    processing_month = dataprep.events_df['processing_month'].max()
    start_month = pd.to_datetime(
        filtered_events_df['event_start_utc'].astype('str').str[:8]
    ).dt.strftime('%Y-%m').values[0]

    date_range = pd.DataFrame(
        index=pd.date_range(
        start=start_month, 
        end=processing_month,
        freq='MS')
    )
    date_range['processing_month'] = date_range.index.strftime('%Y-%m')

    event_start_dt = pd.to_datetime(
        filtered_events_df['event_start_utc'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d').values[0]
    event_end_dt = pd.to_datetime(
        filtered_events_df['event_end_utc'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d').values[0]


    edits_ts = date_range.merge(
        filtered_contributions_df.groupby('processing_month')[dataprep.unreverted_edit_counts_columns].sum(),
        how='left',
        on='processing_month'
    ).set_index('processing_month')[dataprep.unreverted_edit_counts_columns].fillna(0)


    custom_layout = go.Layout(
        yaxis=dict(
            title='Edits by participants',
            showgrid=False,
            rangemode='tozero'
        ),
        legend=dict(
            orientation="h",

            yanchor="top",
            y=-0.12,
            xanchor="left",
            x=0.0
        ),

        barmode='stack'
    )


    # Create figure
    fig = go.Figure()

    for x in dataprep.unreverted_edit_counts_columns[:-1]:
        trace = go.Bar(
            x=edits_ts.index, 
            y=edits_ts[x],
            name=x[20:-7].replace('_', '-') + ' edits',
        )
        fig.add_trace(trace)



    # Add shape region
    fig.add_vrect(
        x0=event_start_dt, 
        x1=event_end_dt, 
        fillcolor=COLORS['chart_area_highlight'], 
        opacity=0.5,
        layer="below", 
        #line_width=0,
        line_width=2,
        line_color=COLORS['chart_area_highlight'],

        annotation_text="event duration", 
        annotation_position="top left",
    )
    fig.update_layout(hovermode='x unified')

    fig.update_layout(base_time_series_chart_layout())
    fig.update_layout(custom_layout)
    
    return fig.to_html(full_html=False)


# helper function
def get_detail_stat(s, censoring_label='<100'):
    '''Expected input is a series.'''
    r = None
    
    if config.LOCAL_DEV_MODE:
        stat = pd.to_numeric(s).sum()
        r = f"{stat:,.0f}"

    else:
        unique_vals = s.unique()
        # If the list of unique values is just one censored string value, then return it,
        # otherwise, replace the censored string values with nan and sum the rest.
        
        if len(unique_vals) == 1 and type(unique_vals[0]) == str and '<' in unique_vals[0]:
            r = censoring_label #unique_vals[0]
        else:
            stat = pd.to_numeric(s.replace(r'<.*', np.nan, regex=True)).sum()
            if stat == 0 and censoring_label:
                r = censoring_label
            else:
                r = f"{stat:,.0f}"

    return r




def event_stats_table(filtered_contributions_df, filtered_events_df):

    stats_labels = [
        'organizers', 
        'participants', 
        'all edits',
        'edits by first-time editors',
        'edits by beginner editors',
        'edits by intermediate editors',
        'edits by expert editors',
        'edits by super editors',
        'all edits bytes',
        'pages created',

    ]

    stats_values_array = [
        get_detail_stat(filtered_events_df['organizers_count'], censoring_label=None),
        get_detail_stat(filtered_events_df['participants_count'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['unreverted_edits_count'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['reverted_edits_by_first_time_editor'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['unreverted_edits_by_beginner_editor'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['unreverted_edits_by_intermediate_editor'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['unreverted_edits_by_expert_editor'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['unreverted_edits_by_super_editor'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['edits_bytes'], censoring_label=None),
        get_detail_stat(filtered_contributions_df['pages_created_count'], censoring_label=None),
    ]

    stats = pd.DataFrame(index=stats_labels, data={'count': stats_values_array}).fillna(0) 
    
    return stats.to_html(index_names=False, justify='right', classes='table')




def event_influence_chart3(event_details, event_influence_table):

    fig = go.Figure()

    ticksuffix = "  "

    # add a trace for the selected event
    # one trace per one event
    event_id = str(event_details['event_id'])
    event_name = event_details['event_name']
    event_start = event_details['event_start_utc_dt']#.strftime('%Y-%m-%d')
    event_end = event_details['event_end_utc_dt']#.strftime('%Y-%m-%d')
    event_category = 'same organizer'.replace(' ', ticksuffix + '<br>') + ticksuffix
    event_participants_count = event_details['participants_count']
    event_organizers_count = event_details['organizers_count']

    event_end_display = event_end
    if event_start == event_end:
        event_end_display = datetime.strptime(event_end, '%Y-%m-%d %H:%M') + timedelta(days=1) - timedelta(hours=2)

    event_tooltip_text = (
        f'{event_name}<br>Event id: {event_id}'
        + f'<br>Start: {event_start[:-5]}<br>End: {event_start[:-5]}'
        + f'<br>Organizers: {event_organizers_count}<br>Participants: {event_participants_count}'
    )

    participants_count_max = max(event_participants_count, event_influence_table['participants_count'].max())
    participants_count_max_width = 60

    trace = go.Scatter(
        y=[event_category, event_category],
        x=[event_start, event_end_display],
        mode='lines',
        line=dict(
            color='#ffa15a',
            width=event_participants_count / participants_count_max * participants_count_max_width
        ),
        hovertext=[event_tooltip_text, event_tooltip_text],
        hoverinfo="text",
        name='selected event'
    )

    fig.add_trace(trace)
    
    # add traces for related events
    for i,r in event_influence_table.reset_index().iterrows():
        # one trace per one event
        event_id = str(r['event_id'])
        event_name = r['event_name']
        event_start = r['event_start_utc_dt'].strftime('%Y-%m-%d')
        event_end = r['event_end_utc_dt'].strftime('%Y-%m-%d')
        event_category = r['subseq_event_by'].replace(' ', ticksuffix + '<br>') + ticksuffix
        event_participants_count = int(r['participants_count'])
        event_organizers_count = r['organizers_count']
        event_organizers_in_common_count = r['organizers_in_common_count']
        event_participants_in_common_count = r['participants_in_common_count']
        event_participants_turned_organizers_count = r['participants_turned_organizers_count']
        
        event_end_display = event_end
        if event_start == event_end:
            event_end_display = datetime.strptime(event_end, '%Y-%m-%d') + timedelta(days=1) - timedelta(hours=2)

        event_tooltip_text = (
            f'{event_name}<br>Event id: {event_id}' 
            + f'<br>Start: {event_start}<br>End: {event_start}'
            + f'<br>Organizers: {event_organizers_count}<br>Participants: {event_participants_count}'
            + f'<br>Organizers in common: {event_organizers_in_common_count}'
            + f'<br>Participants in common: {event_participants_in_common_count}'
            + f'<br>Participants turned organizers: {event_participants_turned_organizers_count}'
        )

        showlegend_flag = True if i == 0 else False

        trace = go.Scatter(
            y=[event_category, event_category],
            x=[event_start, event_end_display],
            mode='lines',
            line=dict(
                color='#636efb',
                width=event_participants_count / participants_count_max * participants_count_max_width
            ),
            hovertext=[event_tooltip_text, event_tooltip_text],
            hoverinfo="text",
            opacity=0.7,
            showlegend=showlegend_flag,
            name=f'related events'
        )
    
        fig.add_trace(trace)


    # Add annotation below the plot
    fig.add_annotation(
        text='Bar height = number of participants', 
        align='left',
        showarrow=False,
        xref='paper',
        yref='paper',
        x=0.01,
        y=1.07,
    )



    subseq_events_organizers = ['both', 'participant turned organizer', 'same organizer']
    subseq_events_organizers_display = [ x.replace(' ', ticksuffix + '<br>') + ticksuffix for x in subseq_events_organizers ]

    #yrange = [-1, len(subseq_events_organizers)]
    yrange = [-1, 3]

    custom_layout = go.Layout(

        yaxis_range=yrange,
        
        yaxis=dict(
            gridcolor='#eaecf0',
            categoryorder='array',
            categoryarray= subseq_events_organizers_display
        ),
        legend=dict(
            orientation="h",
            yanchor="top",
            y=1.15,
            xanchor="left",
            x=0.0
        ),
        showlegend=True,

        height=400,
    )


    fig.update_layout(base_time_series_chart_layout())
    fig.update_layout(custom_layout)

    return fig.to_html(full_html=False)




def event_influence_table(event_id, events_df, filtered_events_graph_edges_df):

    selected_edges_cols = [
        'event_id', 
        'organizers_in_common_count', 
        'participants_in_common_count',
        'participants_turned_organizers_count',
        'subseq_event_by'
    ]
    selected_events_cols = [
        'event_id',
        'event_name',
        'event_meeting_type',
        'grant_funded_event', 
        'event_start_utc', 
        'event_end_utc',
        'organizers_count',
        'participants_count'
    ]

    filtered_events_graph_edges_df = filtered_events_graph_edges_df[selected_edges_cols].merge(
        events_df[selected_events_cols],
        how='left',
        on='event_id'
    )
    

    # Filter to subseq events
    filtered_events_graph_edges_df['event_start_utc_dt'] = pd.to_datetime(
        filtered_events_graph_edges_df['event_start_utc'].astype('str').str[:8]
    ) #.dt.strftime('%Y-%m-%d')
    filtered_events_graph_edges_df['event_end_utc_dt'] = pd.to_datetime(
        filtered_events_graph_edges_df['event_end_utc'].astype('str').str[:8]
    ) #.dt.strftime('%Y-%m-%d')

    selected_event_details = events_df[events_df['event_id'] == event_id]
    selected_event_start_utc_dt = pd.to_datetime(
        selected_event_details['event_start_utc'].astype('str').str[:8]
    ).values[0] #.dt.strftime('%Y-%m-%d')
    selected_event_end_utc_dt = pd.to_datetime(
        selected_event_details['event_end_utc'].astype('str').str[:8]
    ).values[0] #.dt.strftime('%Y-%m-%d')


    filtered_events_graph_edges_df['event_relative_timing'] = np.where(
        filtered_events_graph_edges_df['event_end_utc_dt'] < selected_event_start_utc_dt,
        'prior',
        np.where(
            filtered_events_graph_edges_df['event_start_utc_dt'] > selected_event_end_utc_dt,
            'subsequent',
            'concurrent'
        )
    )

    # filter out related events that don't have organizers or organizer-turned participants in common
    filtered_events_graph_edges_df = filtered_events_graph_edges_df[filtered_events_graph_edges_df['subseq_event_by'] != '']

    # Drop duplicates #TODO: investigate this
    filtered_events_graph_edges_df.drop_duplicates(subset=['event_id'], inplace=True)

    return filtered_events_graph_edges_df





def grant_participants_chart(filtered_contributions_df, filtered_events_df):

    # date range for event charts
    # starts at event start and ends at processing month
    processing_month = dataprep.events_df['processing_month'].max()
    start_month = pd.to_datetime(
        filtered_events_df['grant_agreement_dt']
    ).dt.strftime('%Y-%m').values[0]

    date_range = pd.DataFrame(
        index=pd.date_range(
        start=start_month, 
        end=processing_month,
        freq='MS')
    )
    date_range['processing_month'] = date_range.index.strftime('%Y-%m')

    grant_start_dt = pd.to_datetime(
        filtered_events_df['grant_agreement_dt']
    ).dt.strftime('%Y-%m-%d').values[0]
    grant_end_dt = date_range.index.max().strftime('%Y-%m-%d')


    edits_ts = date_range.merge(
        filtered_contributions_df.groupby('processing_month')[dataprep.unreverted_edit_counts_columns].sum(),
        how='left',
        on='processing_month'
    ).set_index('processing_month')[dataprep.unreverted_edit_counts_columns].fillna(0)


    custom_layout = go.Layout(
        yaxis=dict(
            title='Edits by participants',
            showgrid=False,
            rangemode='tozero'
        ),
        legend=dict(
            orientation="h"
        ),

        barmode='stack'
    )


    # Create figure
    fig = go.Figure()

    for x in dataprep.unreverted_edit_counts_columns[:-1]:
        trace = go.Bar(
            x=edits_ts.index, 
            y=edits_ts[x],
            name=x[20:-7].replace('_', '-') + ' edits',
        )
        fig.add_trace(trace)



    #Add shape region
    fig.add_vrect(
        x0=grant_start_dt, 
        x1=grant_start_dt, 
        fillcolor=COLORS['chart_area_highlight'], 
        opacity=0.5,
        layer="below", 
        line_width=2,
        line_color=COLORS['chart_area_highlight'],

        annotation_text="grant issued", 
        annotation_position="top left",
    )

    fig.update_layout(hovermode='x unified')

    fig.update_layout(base_time_series_chart_layout())
    fig.update_layout(custom_layout)
    
    return fig.to_html(full_html=False)





def grant_stats_table(filtered_contributions_df, filtered_events_df):

    stats_labels = [
        'events',
        'organizers', 
        'participants', 
        'all edits',
        'edits by first-time editors',
        'edits by beginner editors',
        'edits by intermediate editors',
        'edits by expert editors',
        'edits by super editors',
        'all edits bytes',
        'pages created',

    ]


    stats_values_array = [
        filtered_events_df['event_id'].nunique(),
        get_detail_stat(filtered_events_df['organizers_count'], censoring_label='censored'),
        get_detail_stat(filtered_events_df['participants_count'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['unreverted_edits_count'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['reverted_edits_by_first_time_editor'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['unreverted_edits_by_beginner_editor'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['unreverted_edits_by_intermediate_editor'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['unreverted_edits_by_expert_editor'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['unreverted_edits_by_super_editor'], censoring_label='censored'),
        get_detail_stat(filtered_contributions_df['edits_bytes'], censoring_label=None),
        get_detail_stat(filtered_contributions_df['pages_created_count'], censoring_label=None),
    ]

    stats = pd.DataFrame(index=stats_labels, data={'count': stats_values_array}).fillna(0) 

    return stats.to_html(index_names=False, justify='right', classes='table')





def grant_influence_table(grant_id, events_df, filtered_events_graph_edges_df):

    selected_edges_cols = [
        'event_id', 
        'organizers_in_common_count', 
        'participants_in_common_count',
        'participants_turned_organizers_count',
        'subseq_event_by'
    ]
    selected_events_cols = [
        'grant_id',
        'event_id',
        'event_name',
        'event_meeting_type',
        'grant_funded_event', 
        'event_start_utc', 
        'event_end_utc',
        'organizers_count',
        'participants_count'
    ]

    filtered_events_graph_edges_df = filtered_events_graph_edges_df[selected_edges_cols].merge(
        events_df[selected_events_cols],
        how='left',
        on='event_id',
    )
    

    # Filter to subseq events
    filtered_events_graph_edges_df['event_start_utc_dt'] = pd.to_datetime(
        filtered_events_graph_edges_df['event_start_utc'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d')
    filtered_events_graph_edges_df['event_end_utc_dt'] = pd.to_datetime(
        filtered_events_graph_edges_df['event_end_utc'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d')

    # selected_event_details = events_df[events_df['event_id'] == event_id]
    # selected_event_start_utc_dt = pd.to_datetime(
    #     selected_event_details['event_start_utc'].astype('str').str[:8]
    # ).values[0] #.dt.strftime('%Y-%m-%d')
    # selected_event_end_utc_dt = pd.to_datetime(
    #     selected_event_details['event_end_utc'].astype('str').str[:8]
    # ).values[0] #.dt.strftime('%Y-%m-%d')


    # filtered_events_graph_edges_df['event_relative_timing'] = np.where(
    #     filtered_events_graph_edges_df['event_end_utc_dt'] < selected_event_start_utc_dt,
    #     'prior',
    #     np.where(
    #         filtered_events_graph_edges_df['event_start_utc_dt'] > selected_event_end_utc_dt,
    #         'subsequent',
    #         'concurrent'
    #     )
    # )

    # filter out related events that don't have organizers or organizer-turned participants in common
    filtered_events_graph_edges_df = filtered_events_graph_edges_df[filtered_events_graph_edges_df['subseq_event_by'] != '']

    # Drop duplicates #TODO: investigate this
    filtered_events_graph_edges_df.drop_duplicates(subset=['event_id'], inplace=True)

    return filtered_events_graph_edges_df









def grant_influence_chart(
    funded_event_influence_table, 
    other_funded_event_influence_table,
    unfunded_event_influence_table):

    fig = go.Figure()

    ticksuffix = "  "
    participants_count_max_width = 60
    participants_count_max = max(
        funded_event_influence_table['participants_count'].max(),
        other_funded_event_influence_table['participants_count'].max(),
        unfunded_event_influence_table['participants_count'].max(),
    )

    # add traces for the events funded by the selected grant
    for i,r in funded_event_influence_table.reset_index().iterrows():
        # one trace per one event
        event_id = str(r['event_id'])
        event_name = r['event_name']
        event_start = r['event_start_utc_dt']#.strftime('%Y-%m-%d')
        event_end = r['event_end_utc_dt']#.strftime('%Y-%m-%d')
        event_category = 'funded event organizer'.replace(' ', ticksuffix + '<br>') + ticksuffix
        event_participants_count = r['participants_count']
        event_organizers_count = r['organizers_count']

        event_end_display = event_end
        if event_start == event_end:
            event_end_display = datetime.strptime(event_end, '%Y-%m-%d %H:%M') + timedelta(days=1) - timedelta(hours=2)

        event_tooltip_text = (
            f'{event_name}<br>Event id: {event_id}'
            + f'<br>Start: {event_start[:-5]}<br>End: {event_start[:-5]}'
            + f'<br>Organizers: {event_organizers_count}<br>Participants: {event_participants_count}'
        )

        #participants_count_max = max(event_participants_count, event_influence_table['participants_count'].max())
        
        showlegend_flag = True if i == 0 else False

        trace = go.Scatter(
            y=[event_category, event_category],
            x=[event_start, event_end_display],
            mode='lines',
            line=dict(
                color='#ffa15a',
                width=event_participants_count / participants_count_max * participants_count_max_width
            ),
            hovertext=[event_tooltip_text, event_tooltip_text],
            hoverinfo="text",
            opacity=0.7,
            showlegend=showlegend_flag,
            name='funded by this grant'
        )

        fig.add_trace(trace)

    
    # add traces for related events funded by other grants
    for i,r in other_funded_event_influence_table.reset_index().iterrows():
        # one trace per one event
        event_id = str(r['event_id'])
        event_name = r['event_name']
        event_start = r['event_start_utc_dt']#.strftime('%Y-%m-%d')
        event_end = r['event_end_utc_dt']#.strftime('%Y-%m-%d')
        event_category = r['subseq_event_by'].replace('same organizer', 'funded event organizer').replace(' ', ticksuffix + '<br>') + ticksuffix
        event_participants_count = int(r['participants_count'])
        event_organizers_count = r['organizers_count']
        event_organizers_in_common_count = r['organizers_in_common_count']
        event_participants_in_common_count = r['participants_in_common_count']
        event_participants_turned_organizers_count = r['participants_turned_organizers_count']
        
        event_end_display = event_end
        if event_start == event_end:
            event_end_display = datetime.strptime(event_end, '%Y-%m-%d') + timedelta(days=1) - timedelta(hours=2)

        event_tooltip_text = (
            f'{event_name}<br>Event id: {event_id}' 
            + f'<br>Start: {event_start}<br>End: {event_start}'
            + f'<br>Organizers: {event_organizers_count}<br>Participants: {event_participants_count}'
            + f'<br>Organizers in common: {event_organizers_in_common_count}'
            + f'<br>Participants in common: {event_participants_in_common_count}'
            + f'<br>Participants turned organizers: {event_participants_turned_organizers_count}'
        )

        showlegend_flag = True if i == 0 else False

        trace = go.Scatter(
            y=[event_category, event_category],
            x=[event_start, event_end_display],
            mode='lines',
            line=dict(
                color='#636efb',
                width=event_participants_count / participants_count_max * participants_count_max_width
            ),
            hovertext=[event_tooltip_text, event_tooltip_text],
            hoverinfo="text",
            opacity=0.7,
            showlegend=showlegend_flag,
            name=f'funded by other grants'
        )
    
        fig.add_trace(trace)




    # add traces for related unfunded events 
    for i,r in unfunded_event_influence_table.reset_index().iterrows():
        # one trace per one event
        event_id = str(r['event_id'])
        event_name = r['event_name']
        event_start = r['event_start_utc_dt']#.strftime('%Y-%m-%d')
        event_end = r['event_end_utc_dt']#.strftime('%Y-%m-%d')
        event_category = r['subseq_event_by'].replace('same organizer', 'funded event organizer').replace(' ', ticksuffix + '<br>') + ticksuffix
        event_participants_count = int(r['participants_count'])
        event_organizers_count = r['organizers_count']
        event_organizers_in_common_count = r['organizers_in_common_count']
        event_participants_in_common_count = r['participants_in_common_count']
        event_participants_turned_organizers_count = r['participants_turned_organizers_count']
        
        event_end_display = event_end
        if event_start == event_end:
            event_end_display = datetime.strptime(event_end, '%Y-%m-%d') + timedelta(days=1) - timedelta(hours=2)

        event_tooltip_text = (
            f'{event_name}<br>Event id: {event_id}' 
            + f'<br>Start: {event_start}<br>End: {event_start}'
            + f'<br>Organizers: {event_organizers_count}<br>Participants: {event_participants_count}'
            + f'<br>Organizers in common: {event_organizers_in_common_count}'
            + f'<br>Participants in common: {event_participants_in_common_count}'
            + f'<br>Participants turned organizers: {event_participants_turned_organizers_count}'
        )

        showlegend_flag = True if i == 0 else False

        trace = go.Scatter(
            y=[event_category, event_category],
            x=[event_start, event_end_display],
            mode='lines',
            line=dict(
                color='#00cc96',
                width=event_participants_count / participants_count_max * participants_count_max_width
            ),
            hovertext=[event_tooltip_text, event_tooltip_text],
            hoverinfo="text",
            opacity=0.7,
            showlegend=showlegend_flag,
            name=f'unfunded events'
        )
    
        fig.add_trace(trace)





    # Add annotation below the plot
    fig.add_annotation(
        text='Bar height = number of participants', 
        align='left',
        showarrow=False,
        xref='paper',
        yref='paper',
        x=0.01,
        y=1.07,
    )



    subseq_events_organizers = ['both', 'participant turned organizer', 'funded event organizer']
    subseq_events_organizers_display = [ x.replace(' ', ticksuffix + '<br>') + ticksuffix for x in subseq_events_organizers ]

    #yrange = [-1, len(subseq_events_organizers)]
    yrange = [-1, 3]

    custom_layout = go.Layout(

        yaxis_range=yrange,
        
        yaxis=dict(
            gridcolor='#eaecf0',
            categoryorder='array',
            categoryarray= subseq_events_organizers_display
        ),
        legend=dict(
            orientation="h",
            # yanchor="top",
            # y=1.15,
            # xanchor="left",
            # x=0.0
            yanchor="top",
            y=-0.15,
            xanchor="left",
            x=0.0
        ),
        showlegend=True,

        height=400,
    )


    fig.update_layout(base_time_series_chart_layout())
    fig.update_layout(custom_layout)

    return fig.to_html(full_html=False)

