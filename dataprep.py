import pandas as pd
import numpy as np

from ast import literal_eval

import dataimports


# Data imports
# test = pd.read_csv('https://analytics.wikimedia.org/published/datasets/periodic/reports/metrics/wmcs/wikis_by_wmcs_edits.tsv', sep='\t')
# test = pd.read_csv('https://analytics.wikimedia.org/published/datasets/events_impact_reporting/events/events_2024_07_ended.csv')
print('\ndataimports.events_df.columns: ')
print(dataimports.events_df.columns)
print('\ndataimports.event_contributions_df.columns: ')
print(dataimports.event_contributions_df.columns)
print('\ndataimports.events_graph_edges_df.columns: ')
print(dataimports.events_graph_edges_df.columns)


events_df = dataimports.events_df
# Convert grant_country_wikimedia_region field to string for displays, leave missing values as nan.
events_df['grant_country_wikimedia_region'] = events_df['grant_country_wikimedia_region']\
    .fillna('[]').apply(literal_eval).apply(lambda x: ', '.join(x)).replace('', np.nan)
# Shorten grant_country_list_standardized for displays
country_abbr = {
    'Democratic Republic of the Congo': 'DRC'
}
events_df['grant_country_list_standardized'] = events_df['grant_country_list_standardized'].apply(lambda x: country_abbr[x] if x in country_abbr else x)


# Merge grant data fields from the events table onto the contributions table
# This is needed for filtering.
contributions_df = dataimports.event_contributions_df.merge(
    events_df[['event_id', 'grant_id', 'grant_country_list', 'grant_country_list_standardized', 'grant_country_wikimedia_region']],
    how='left',
    left_on='campaign_event_id',
    right_on='event_id'
).drop(columns='event_id')


# Add wiki project names to the events table
events2wiki_dbs_df = contributions_df[['campaign_event_id', 'wiki_db']].groupby('campaign_event_id').agg(['unique']).reset_index()
events2wiki_dbs_df.columns = ['event_id', 'wiki_dbs_list']
events_df = events_df.merge(
    events2wiki_dbs_df,
    how='left',
    on='event_id'
)

# Add a parsed out grant agreement month field
def date_str_to_month(date_str):
    r =''
    if date_str != '':
        year = date_str[:4]
        month = date_str[4:6]
        r = f'{year}-{month}'
    return r
events_df['grant_agreement_month'] = events_df['grant_agreement_dt'].fillna('').astype(str).apply(date_str_to_month)





# contributions_df = pd.read_csv('contributions.csv')
# events_df = pd.read_csv('events.csv')


# Modify contributions df
edit_counts_columns = [
    'edits_by_first_time_editor',
    'edits_by_beginner_editor', 
    'edits_by_intermediate_editor',
    'edits_by_expert_editor', 
    'edits_by_super_editor', 
    'edits_count'
]
reverted_edit_counts_columns = [ 'reverted_' + x for x in edit_counts_columns ]
unreverted_edit_counts_columns = [ 'unreverted_' + x for x in edit_counts_columns ]

contributions_df['processing_year'] = contributions_df['processing_month'].str[:4].astype(int)

for x in edit_counts_columns:
    contributions_df['unreverted_' + x] = (
        pd.to_numeric(contributions_df[x], errors='coerce') 
        - pd.to_numeric(contributions_df['reverted_' + x], errors='coerce') 
    )
contributions_df['grant_funded_event'] = np.where(contributions_df['grant_id'].notna(), True, False)
    

# Modify events df
events_df['grant_funded_event'] = np.where(events_df['grant_id'].notna(), True, False)


# Event networks df
#events_graph_edges_df = pd.read_csv('events_graph_edges.csv')
events_graph_edges_df = dataimports.events_graph_edges_df

# Add subseq_event_by, a string field indicating whether the subseq event was organized by the same organizer,
# a psrticipant turned organizer, or both
def get_subseq_event_by(organizers_in_common_count, participants_turned_organizers_count):
    r = ''
    if organizers_in_common_count > 0 and participants_turned_organizers_count > 0:
        r = 'both'
    elif organizers_in_common_count > 0:
        r = 'same organizer'
    elif participants_turned_organizers_count > 0:
        r = 'participant turned organizer'

    return r

events_graph_edges_df['subseq_event_by'] = events_graph_edges_df.apply(
    lambda x: get_subseq_event_by(x['organizers_in_common_count'], x['participants_turned_organizers_count']), 
    axis=1
)

# Add event timestamps
# from_event side
events_graph_edges_df = events_graph_edges_df.merge(
    events_df[['event_id', 'event_start_utc', 'event_end_utc']],
    how='left',
    left_on='from_event_id',
    right_on='event_id'
).rename(columns={
    'event_start_utc': 'from_event_start_utc',
    'event_end_utc': 'from_event_end_utc',
})

# to_event side
events_graph_edges_df = events_graph_edges_df.merge(
    events_df[['event_id', 'event_start_utc', 'event_end_utc']],
    how='left',
    left_on='to_event_id',
    right_on='event_id'
).rename(columns={
    'event_start_utc': 'to_event_start_utc',
    'event_end_utc': 'to_event_end_utc',
})