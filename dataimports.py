import pandas as pd
import numpy as np

import io
import requests
from requests.adapters import HTTPAdapter
from urllib3.util.retry import Retry


import config

print(f'\nLOCAL_DEV_MODE = {config.LOCAL_DEV_MODE}')

if not config.LOCAL_DEV_MODE:
    # Create a requests session with retry configuration
    session = requests.Session()
    retry = Retry(total=3, backoff_factor=0.1, status_forcelist=[500, 502, 503, 504])
    adapter = HTTPAdapter(max_retries=retry)
    session.mount('http://', adapter)
    session.mount('https://', adapter)



# Setup
PUBLIC_DATASETS_URL = 'https://analytics.wikimedia.org/published/datasets/events_impact_reporting/'
START_YEAR_MONTH_SNAKE = config.START_YEAR_MONTH.replace('-','_')
PROCESSING_YEAR_MONTH_SNAKE = config.PROCESSING_YEAR_MONTH.replace('-','_')
print(f'\nProcessing month = {PROCESSING_YEAR_MONTH_SNAKE}\n')



start_year, start_month = [ int(x) for x in START_YEAR_MONTH_SNAKE.split('_')]
processing_year, processing_month = [ int(x) for x in PROCESSING_YEAR_MONTH_SNAKE.split('_')]


def get_public_data(url):
    r = None
    try:
        url_data = session.get(url).content
        r = pd.read_csv(io.StringIO(url_data.decode('utf-8')))
    except:
        print(f'ERROR in dataimports.py: Cannot access {url}')
    return r


def get_csv_files_events_and_edges(year, month, suffix):
    if config.LOCAL_DEV_MODE:
        events_df = pd.read_csv(f'{config.LOCAL_DATASETS_FOLDER}/events_{year}_{month:02}_{suffix}.csv')
        events_graph_edges_df = pd.read_csv(f'{config.LOCAL_DATASETS_FOLDER}/events_graph_edges_{year}_{month:02}_{suffix}.csv')
    else:
        events_url = f'{PUBLIC_DATASETS_URL}events/events_{year}_{month:02}_{suffix}.csv'
        events_df = get_public_data(events_url)
    
        events_graph_edges_url = f'{PUBLIC_DATASETS_URL}events_graph_edges/events_graph_edges_{year}_{month:02}_{suffix}.csv'
        events_graph_edges_df = get_public_data(events_graph_edges_url)

    return events_df, events_graph_edges_df


def get_csv_files_contributions(year, month):
    if config.LOCAL_DEV_MODE:
        event_contributions_df = pd.read_csv(f'{config.LOCAL_DATASETS_FOLDER}/event_contributions_{year}_{month:02}.csv')
    else:
        event_contributions_url = f'{PUBLIC_DATASETS_URL}event_contributions/event_contributions_{year}_{month:02}.csv'
        event_contributions_df = get_public_data(event_contributions_url)

    return event_contributions_df


# Data imports

for year in range(start_year, processing_year+1):
    if start_year == processing_year:
        for month in range(start_month, processing_month+1):
            if month == start_month:
                event_contributions_df = get_csv_files_contributions(year, month)
                # get the ENDED files
                events_df, events_graph_edges_df = get_csv_files_events_and_edges(year, month, 'ended')
            elif month == processing_month:
                event_contributions_df = pd.concat([event_contributions_df, get_csv_files_contributions(year, month)])
                # get both ENDED and ONGOING files
                ## ENDED files
                events_df_temp, events_graph_edges_df_temp = get_csv_files_events_and_edges(year, month, 'ended')
                events_df = pd.concat([events_df, events_df_temp])
                events_graph_edges_df = pd.concat([events_graph_edges_df, events_graph_edges_df_temp])
                del events_df_temp, events_graph_edges_df_temp

                ## ONGOING files
                events_df_temp, events_graph_edges_df_temp = get_csv_files_events_and_edges(year, month, 'ongoing')
                events_df = pd.concat([events_df, events_df_temp])
                events_graph_edges_df = pd.concat([events_graph_edges_df, events_graph_edges_df_temp])
                del events_df_temp, events_graph_edges_df_temp
            else:
                event_contributions_df = pd.concat([event_contributions_df, get_csv_files_contributions(year, month)])
                # get the ENDED files
                events_df_temp, events_graph_edges_df_temp = get_csv_files_events_and_edges(year, month, 'ended')
                events_df = pd.concat([events_df, events_df_temp])
                events_graph_edges_df = pd.concat([events_graph_edges_df, events_graph_edges_df_temp])
                del events_df_temp, events_graph_edges_df_temp

            #print(f'{year}-{month:02}')
            print(f'\nCollected the events_{year}_{month:02} data. Total number of records collected = {len(events_df)}.')
            print(f'Collected the event_contributions_{year}_{month:02} data. Total number of records collected = {len(event_contributions_df)}.')
            print(f'Collected the events_graph_edges_{year}_{month:02} data. Total number of records collected = {len(events_graph_edges_df)}.')
    else:
        if year == start_year:
            for month in range(start_month, 13):
                if month == start_month:
                    event_contributions_df = get_csv_files_contributions(year, month)
                    # get the ENDED files
                    events_df, events_graph_edges_df = get_csv_files_events_and_edges(year, month, 'ended')
                else:
                    event_contributions_df = pd.concat([event_contributions_df, get_csv_files_contributions(year, month)])
                    # get the ENDED files
                    events_df_temp, events_graph_edges_df_temp = get_csv_files_events_and_edges(year, month, 'ended')
                    events_df = pd.concat([events_df, events_df_temp])
                    events_graph_edges_df = pd.concat([events_graph_edges_df, events_graph_edges_df_temp])
                    del events_df_temp, events_graph_edges_df_temp

                #print(f'{year}-{month:02}')
                print(f'\nCollected the events_{year}_{month:02} data. Total number of records collected = {len(events_df)}.')
                print(f'Collected the event_contributions_{year}_{month:02} data. Total number of records collected = {len(event_contributions_df)}.')
                print(f'Collected the events_graph_edges_{year}_{month:02} data. Total number of records collected = {len(events_graph_edges_df)}.')
        elif year == processing_year:
            for month in range(1, processing_month+1):
                if month == processing_month:
                    event_contributions_df = pd.concat([event_contributions_df, get_csv_files_contributions(year, month)])
                    # get both ENDED and ONGOING files
                    ## ENDED files
                    events_df_temp, events_graph_edges_df_temp = get_csv_files_events_and_edges(year, month, 'ended')
                    events_df = pd.concat([events_df, events_df_temp])
                    events_graph_edges_df = pd.concat([events_graph_edges_df, events_graph_edges_df_temp])
                    del events_df_temp, events_graph_edges_df_temp

                    ## ONGOING files
                    events_df_temp, events_graph_edges_df_temp = get_csv_files_events_and_edges(year, month, 'ongoing')
                    events_df = pd.concat([events_df, events_df_temp])
                    events_graph_edges_df = pd.concat([events_graph_edges_df, events_graph_edges_df_temp])
                    del events_df_temp, events_graph_edges_df_temp
                else:
                    event_contributions_df = pd.concat([event_contributions_df, get_csv_files_contributions(year, month)])
                    # get the ENDED files
                    events_df_temp, events_graph_edges_df_temp = get_csv_files_events_and_edges(year, month, 'ended')
                    events_df = pd.concat([events_df, events_df_temp])
                    events_graph_edges_df = pd.concat([events_graph_edges_df, events_graph_edges_df_temp])
                    del events_df_temp, events_graph_edges_df_temp

                #print(f'{year}-{month:02}')
                print(f'\nCollected the events_{year}_{month:02} data. Total number of records collected = {len(events_df)}.')
                print(f'Collected the event_contributions_{year}_{month:02} data. Total number of records collected = {len(event_contributions_df)}.')
                print(f'Collected the events_graph_edges_{year}_{month:02} data. Total number of records collected = {len(events_graph_edges_df)}.')
        else:
            for month in range(1, 13):
                event_contributions_df = pd.concat([event_contributions_df, get_csv_files_contributions(year, month)])
                # get the ENDED files
                events_df_temp, events_graph_edges_df_temp = get_csv_files_events_and_edges(year, month, 'ended')
                events_df = pd.concat([events_df, events_df_temp])
                events_graph_edges_df = pd.concat([events_graph_edges_df, events_graph_edges_df_temp])
                del events_df_temp, events_graph_edges_df_temp

                #print(f'{year}-{month:02}')
                print(f'\nCollected the events_{year}_{month:02} data. Total number of records collected = {len(events_df)}.')
                print(f'Collected the event_contributions_{year}_{month:02} data. Total number of records collected = {len(event_contributions_df)}.')
                print(f'Collected the events_graph_edges_{year}_{month:02} data. Total number of records collected = {len(events_graph_edges_df)}.')

print(f'\nFinished collecting the events_YYYY_MM data. Total number of records collected = {len(events_df)}.')
print(f'Finished collecting the event_contributions_YYYY_MM data. Total number of records collected = {len(event_contributions_df)}.')
print(f'Finished collecting the events_graph_edges_YYYY_MM data. Total number of records collected = {len(events_graph_edges_df)}.')


