# Manually set/edit config variables for report generation here

# The processing month, which is the most recent year-month to include in the report, in YYYY-MM format
# This needs to be updated on every run
PROCESSING_YEAR_MONTH = '2025-01'


# The oldest year-month to include in the report, in YYYY-MM format
# TODO: in the future, calculate this value from PROCESSING_MONTH
START_YEAR_MONTH = '2024-01'


# If set to True, local data files will be used; otherwise, the censored public data files will be used.
# This setting musht be set to False when deploying on Toolforge.
# Set to True for local development and testing.
LOCAL_DEV_MODE = False 

# Folder that contains local input datasets, need for local development when LOCAL_DEV_MODE is set to True
LOCAL_DATASETS_FOLDER = 'output'