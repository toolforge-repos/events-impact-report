import flask
from flask import Flask, request, jsonify, render_template, redirect, url_for
from flask_wtf import CSRFProtect

from flask_bootstrap import Bootstrap5

import secrets

import plotly.graph_objs as go
from plotly.subplots import make_subplots

import json
import pandas as pd
import numpy as np

import dataprep, forms, helpers, dataviz

app = Flask(__name__)

# For Bootstrap-Flask
bootstrap = Bootstrap5(app)
# For Flask-WTF
csrf = CSRFProtect(app)

# Needed for Flask-WTF
app.secret_key = '12345' #secrets.token_urlsafe(16) # TODO: use env var for this



# Mappings
wiki_db_names = dataprep.contributions_df[['wiki_db', 'wiki_name']].drop_duplicates()
mappings = {}
mappings['wiki_db2name_lookup'] = wiki_db_names.set_index('wiki_db').to_dict(orient='dict')['wiki_name']
mappings['wiki_name2db_lookup'] = wiki_db_names.set_index('wiki_name').to_dict(orient='dict')['wiki_db']

# Filter options
wiki_projects = ['All'] + sorted(list(dataprep.contributions_df['wiki_name'].unique()))
grant_funding_status_options = ['All', 'Funded', 'Not funded']
grant_regions = ['All', 'N/A', 'Unknown'] + sorted(list(dataprep.contributions_df['grant_country_wikimedia_region'].dropna().unique()))
grant_countries = ['All', 'N/A', 'Unknown'] + sorted(list(dataprep.contributions_df['grant_country_list_standardized'].dropna().unique()))

print(grant_regions)


@app.route('/', methods=['GET', 'POST'])
def index():
    filtered_events_df = dataprep.events_df
    filtered_contributions_df = dataprep.contributions_df
    processing_month = dataprep.events_df['processing_month'].max()
    processing_month_display = pd.to_datetime(processing_month).strftime('%B %Y')

    form = forms.FilterForm()
    form.wiki_project.choices = wiki_projects
    form.grant_funding_status.choices=grant_funding_status_options
    form.grant_region.choices=grant_regions
    form.grant_country.choices=grant_countries

    # apply filters to data 
    filtered_events_df, filtered_contributions_df = helpers.apply_filters(
        form, 
        filtered_events_df, 
        filtered_contributions_df,
        mappings
    )


    # data viz
    charts = {}
    tables = {}
    if len(filtered_events_df) > 0:
        charts['events_grants_chart'] = dataviz.events_grants_chart(filtered_events_df)
        charts['participant_activity_chart'] = dataviz.participant_activity_chart(
            filtered_contributions_df, 
            filtered_events_df
        )
        charts['by_country_chart'] = dataviz.by_country_chart(filtered_events_df)
        tables['stats_table'] = dataviz.agg_stats_table(filtered_contributions_df, filtered_events_df)
             
    params={
        'processing_month': processing_month_display,
        'selection': helpers.selection_details(form, filtered_contributions_df, filtered_events_df),
        'wiki_projects': wiki_projects,
        'charts': charts,
        'tables': tables
    }

    return render_template("index.html", params=params, form=form)



@app.route('/events', methods=['GET', 'POST'])
def events():
    filtered_events_df = dataprep.events_df
    filtered_contributions_df = dataprep.contributions_df
    processing_month = dataprep.events_df['processing_month'].max()
    processing_month_display = pd.to_datetime(processing_month).strftime('%B %Y')

    form = forms.FilterForm()
    form.wiki_project.choices = wiki_projects
    form.grant_funding_status.choices=grant_funding_status_options
    form.grant_region.choices=grant_regions
    form.grant_country.choices=grant_countries

    # apply filters to data 
    filtered_events_df, filtered_contributions_df = helpers.apply_filters(
        form, 
        filtered_events_df, 
        filtered_contributions_df,
        mappings
    )

    # dict for selection details and stats
    # selection = dict(
    #     num_events = len(filtered_events_df),
    #     num_edits = filtered_contributions_df['unreverted_edits_count'].sum(),
    #     num_participants = filtered_events_df['participants_count'].sum()
    # )

    events_list_cols = [
        'event_id', 
        'event_name', 
        'event_timezone', 
        'event_start_utc', 
        'event_end_utc', 
        'grant_funded_event', 
        'grant_country_list', 
        'grant_country_list_standardized',
        'organizers_count', 
        'participants_count'
    ]
    events_list = []
    if len(filtered_events_df) > 0:
        events_list_df = filtered_events_df[events_list_cols].copy()
        events_list_df['grant_country_list'].fillna('-', inplace=True)
        events_list_df['grant_country_list_standardized'].fillna('n/a', inplace=True) #.fillna('-', inplace=True)
        events_list_df['event_start_utc'] = pd.to_datetime(
            events_list_df['event_start_utc'].astype('str').str[:8]
        ).dt.strftime('%Y-%m-%d')
        events_list_df['event_end_utc'] = pd.to_datetime(
            events_list_df['event_end_utc'].astype('str').str[:8]
        ).dt.strftime('%Y-%m-%d')
        events_list_df['participants_count'] = events_list_df['participants_count'].fillna(0).astype(int)

        events_list = events_list_df.to_dict('records')

                                
    params={
        'processing_month': processing_month_display,
        'selection': helpers.selection_details(form, filtered_contributions_df, filtered_events_df),
        'data_list': events_list
    }

    return render_template("events.html", params=params, form=form)



@app.route('/grants', methods=['GET', 'POST'])
def grants():
    filtered_events_df = dataprep.events_df
    filtered_contributions_df = dataprep.contributions_df
    processing_month = dataprep.events_df['processing_month'].max()
    processing_month_display = pd.to_datetime(processing_month).strftime('%B %Y')

    form = forms.FilterForm()
    form.wiki_project.choices = wiki_projects
    form.grant_funding_status.choices=grant_funding_status_options
    form.grant_region.choices=grant_regions
    form.grant_country.choices=grant_countries

    # apply filters to data 
    filtered_events_df, filtered_contributions_df = helpers.apply_filters(
        form, 
        filtered_events_df, 
        filtered_contributions_df,
        mappings
    )

    # dict for selection details and stats
    # selection = dict(
    #     num_events = len(filtered_events_df),
    #     num_edits = filtered_contributions_df['unreverted_edits_count'].sum(),
    #     num_participants = filtered_events_df['participants_count'].sum()
    # )
    
    grants_list_cols = [
        'grant_id', 
        'grant_title',
        'grant_agreement_dt',
        'grant_country_list', 
        'grant_country_list_standardized',
        'grant_country_wikimedia_region'
    ]
    grants_list = []
    if len(filtered_events_df) > 0:
        grants_list_df = filtered_events_df.groupby(grants_list_cols)['event_id'].count().reset_index()
        grants_list_df.rename(columns={'event_id': 'events_count'}, inplace=True)
        grants_list_df['grant_country_list'].fillna('-', inplace=True)
        grants_list_df['grant_country_list_standardized'].fillna('n/a', inplace=True) #.fillna('-', inplace=True)
        grants_list_df.sort_values('grant_agreement_dt', ascending=False, inplace=True)
        grants_list_df['grant_agreement_dt'] = pd.to_datetime(
            grants_list_df['grant_agreement_dt'].astype('str').str[:8]
        ).dt.strftime('%Y-%m-%d')

        grants_list = grants_list_df.to_dict('records')
                                
    params={
        'processing_month': processing_month_display,
        'selection': helpers.selection_details(form, filtered_contributions_df, filtered_events_df),
        'data_list': grants_list
    }

    return render_template("grants.html", params=params, form=form)



@app.route('/events/<event_id>', methods=['GET', 'POST'])
def event(event_id):
    event_id = int(event_id)
    
    # event data
    filtered_events_df = dataprep.events_df[dataprep.events_df['event_id'] == event_id]
    filtered_contributions_df = dataprep.contributions_df[dataprep.contributions_df['campaign_event_id'] == event_id]
    filtered_from_events_graph_edges_df = dataprep.events_graph_edges_df[dataprep.events_graph_edges_df['from_event_id'] == event_id]\
        .drop(columns='from_event_id').rename(columns={'to_event_id': 'event_id'})
    filtered_to_events_graph_edges_df = dataprep.events_graph_edges_df[dataprep.events_graph_edges_df['to_event_id'] == event_id]\
        .drop(columns='to_event_id').rename(columns={'from_event_id': 'event_id'})
    filtered_events_graph_edges_df = pd.concat([filtered_from_events_graph_edges_df, filtered_to_events_graph_edges_df])

    filtered_events_df['participants_count'] = filtered_events_df['participants_count'].fillna(0).astype(int)
    filtered_events_df['grant_funded_event_text'] = np.where(filtered_events_df['grant_funded_event'], 'Yes', 'No')


    filtered_events_df['event_start_utc_dt'] = pd.to_datetime(
        filtered_events_df['event_start_utc'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d %H:%M')
    filtered_events_df['event_end_utc_dt'] = pd.to_datetime(
        filtered_events_df['event_end_utc'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d %H:%M')

    

    # event details
    event_details = filtered_events_df.to_dict('records')[0]

    processing_month = dataprep.events_df['processing_month'].max()
    processing_month_display = pd.to_datetime(processing_month).strftime('%B %Y')

    # Update filter options
    wiki_projects = ['All'] + sorted(list(filtered_contributions_df['wiki_name'].unique()))

    form = forms.EventFilterForm()
    form.wiki_project.choices = wiki_projects

    # apply filters to data
    filtered_events_df, filtered_contributions_df = helpers.apply_filters(
        form, 
        filtered_events_df, 
        filtered_contributions_df,
        mappings
    )

    # selection details and stats
    selection = dict(
        wiki_project = form.wiki_project.data,
        num_edits = filtered_contributions_df['unreverted_edits_count'].sum(),
        num_participants = filtered_events_df['participants_count'].fillna(0).astype(int).sum()
    )



    charts = {}
    tables = {}
    if len(filtered_events_df) > 0:
        charts['participant_activity_chart'] = dataviz.event_participants_chart(filtered_contributions_df, filtered_events_df)
        tables['stats_table'] = dataviz.event_stats_table(filtered_contributions_df, filtered_events_df)

        charts['event_influence_chart'] = None
        event_influence_table = dataviz.event_influence_table(event_id, dataprep.events_df, filtered_events_graph_edges_df)
        tables['event_influence_table'] = event_influence_table
        event_details['related_events_flag'] = True
        if len(event_influence_table) == 0:
            event_details['related_events_flag'] = False
        elif len(event_influence_table) < 4:
            tables['prior_event_influence_table'] = event_influence_table[event_influence_table['event_relative_timing'] == 'prior'].to_dict('records')
            tables['concurrent_event_influence_table'] = event_influence_table[event_influence_table['event_relative_timing'] == 'concurrent'].to_dict('records')
            tables['subsequent_event_influence_table'] = event_influence_table[event_influence_table['event_relative_timing'] == 'subsequent'].to_dict('records')
        else:
            charts['event_influence_chart'] = dataviz.event_influence_chart3(event_details, event_influence_table)
        
    params={
        'processing_month': processing_month_display,
        'selection': selection,
        'event_details': event_details,
        'charts': charts,
        'tables': tables
    }

    return render_template("event.html", params=params, form=form)



@app.route('/events/<event_id>/related_events', methods=['GET', 'POST'])
def related_events(event_id):
    event_id = int(event_id)

    processing_month = dataprep.events_df['processing_month'].max()
    processing_month_display = pd.to_datetime(processing_month).strftime('%B %Y')


    filtered_events_df = dataprep.events_df[dataprep.events_df['event_id'] == event_id]
    filtered_events_df['event_start_utc_dt'] = pd.to_datetime(
        filtered_events_df['event_start_utc'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d')
    filtered_events_df['event_end_utc_dt'] = pd.to_datetime(
        filtered_events_df['event_end_utc'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d')
    filtered_events_df['participants_count'] = filtered_events_df['participants_count'].astype(int)
    event_details = filtered_events_df.to_dict('records')[0]


    filtered_from_events_graph_edges_df = dataprep.events_graph_edges_df[dataprep.events_graph_edges_df['from_event_id'] == event_id]\
        .drop(columns='from_event_id').rename(columns={'to_event_id': 'event_id'})
    filtered_to_events_graph_edges_df = dataprep.events_graph_edges_df[dataprep.events_graph_edges_df['to_event_id'] == event_id]\
        .drop(columns='to_event_id').rename(columns={'from_event_id': 'event_id'})
    filtered_events_graph_edges_df = pd.concat([filtered_from_events_graph_edges_df, filtered_to_events_graph_edges_df])


    event_influence_table = dataviz.event_influence_table(event_id, dataprep.events_df, filtered_events_graph_edges_df)
    event_influence_table['event_start_utc_dt'] = event_influence_table['event_start_utc_dt'].dt.strftime('%Y-%m-%d')
    event_influence_table['event_end_utc_dt'] = event_influence_table['event_end_utc_dt'].dt.strftime('%Y-%m-%d')
    event_influence_table.sort_values('event_start_utc_dt', inplace=True)
    
    tables = {}
    tables['event_influence_table'] = event_influence_table.to_dict('records')
        
    tables['prior_event_influence_table'] = event_influence_table[event_influence_table['event_relative_timing'] == 'prior'].to_dict('records')
    tables['concurrent_event_influence_table'] = event_influence_table[event_influence_table['event_relative_timing'] == 'concurrent'].to_dict('records')
    tables['subsequent_event_influence_table'] = event_influence_table[event_influence_table['event_relative_timing'] == 'subsequent'].to_dict('records')


    #events_list = events_list_df.to_dict('records')

                                
    params={
        'processing_month': processing_month_display,
        # 'selection': helpers.selection_details(form, filtered_contributions_df, filtered_events_df),
        #'data_list': events_list,
        'event_details': event_details,
        'tables': tables
    }

    return render_template("related_events.html", params=params)






@app.route('/grants/<grant_id>', methods=['GET', 'POST'])
def grant(grant_id):
    
    # event data
    events_df = dataprep.events_df
    filtered_events_df = events_df[events_df['grant_id'] == grant_id]

    contributions_df = dataprep.contributions_df
    filtered_contributions_df = contributions_df[contributions_df['grant_id'] == grant_id]

    #filtered_events_graph_edges_df = dataprep.events_graph_edges_df[dataprep.events_graph_edges_df['from_event_id'] == grant_id]

    
    filtered_events_df['participants_count'] = filtered_events_df['participants_count'].fillna(0).astype(int)
    filtered_events_df['grant_country_wikimedia_region'] = filtered_events_df['grant_country_wikimedia_region'].fillna('N/A')


    filtered_events_df['event_start_utc_dt'] = pd.to_datetime(
        filtered_events_df['event_start_utc'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d')
    filtered_events_df['event_end_utc_dt'] = pd.to_datetime(
        filtered_events_df['event_end_utc'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d')

    filtered_events_df['grant_agreement_dt'] = pd.to_datetime(
        filtered_events_df['grant_agreement_dt'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d')

    

    # grant details
    grant_details = filtered_events_df.to_dict('records')[0]
    print('TEST =============')
    print(grant_details)

    processing_month = dataprep.events_df['processing_month'].max()
    processing_month_display = pd.to_datetime(processing_month).strftime('%B %Y')

    # Update filter options
    wiki_projects = ['All'] + sorted(list(filtered_contributions_df['wiki_name'].unique()))

    form = forms.EventFilterForm()
    form.wiki_project.choices = wiki_projects

    # apply filters to data
    filtered_events_df, filtered_contributions_df = helpers.apply_filters(
        form, 
        filtered_events_df, 
        filtered_contributions_df,
        mappings
    )

    # selection details and stats
    selection = dict(
        wiki_project = form.wiki_project.data,
        num_edits = filtered_contributions_df['unreverted_edits_count'].sum(),
        num_participants = filtered_events_df['participants_count'].fillna(0).astype(int).sum(),
        num_events = filtered_events_df['event_id'].nunique()
    )



    events_list_cols = [
        'event_id', 
        'event_name', 
        'event_start_utc',  
        'organizers_count', 
        'participants_count'
    ]
    events_list = []
    if len(filtered_events_df) > 0:
        events_list_df = filtered_events_df[events_list_cols].copy()
        events_list_df['event_start_utc'] = pd.to_datetime(
            events_list_df['event_start_utc'].astype('str').str[:8]
        ).dt.strftime('%Y-%m-%d')
        events_list_df['participants_count'] = events_list_df['participants_count'].fillna(0).astype(int)

        events_list = events_list_df.to_dict('records')



    charts = {}
    tables = {}
    if len(filtered_events_df) > 0:
        charts['participant_activity_chart'] = dataviz.grant_participants_chart(filtered_contributions_df, filtered_events_df)
        tables['stats_table'] = dataviz.grant_stats_table(filtered_contributions_df, filtered_events_df)
        tables['events_list_table'] = events_list

        charts['grant_influence_chart'] = None
        #charts['grant_influence_chart'] = dataviz.grant_influence_chart(dataprep.events_df, filtered_events_graph_edges_df)
        
        # grant_influence_table = dataviz.grant_influence_table(grant_id, dataprep.events_df, filtered_events_graph_edges_df)
        # grant_influence_table = filtered_events_graph_edges_df
        # tables['grant_influence_table'] = grant_influence_table
        
        # Edges between events funded by this grant and events funded by other grants
        this_grant_funded_event_ids = list(filtered_events_df['event_id'].unique())
        other_grant_funded_event_ids = list(events_df[
            (events_df['grant_id'] != grant_id)
            & (events_df['grant_funded_event'] == True)
        ]['event_id'].unique())
        events_graph_edges_df = dataprep.events_graph_edges_df
        from_this_funded_to_other_funded_events_graph_edges_df = events_graph_edges_df[
            (events_graph_edges_df['from_event_id'].isin(this_grant_funded_event_ids))
            & (events_graph_edges_df['to_event_id'].isin(other_grant_funded_event_ids))
        ].drop(columns='from_event_id').rename(columns={'to_event_id': 'event_id'})
        from_other_funded_to_this_funded_events_graph_edges_df = events_graph_edges_df[
            (events_graph_edges_df['from_event_id'].isin(other_grant_funded_event_ids))
            & (events_graph_edges_df['to_event_id'].isin(this_grant_funded_event_ids))
        ].drop(columns='to_event_id').rename(columns={'from_event_id': 'event_id'})
        other_funded_events_graph_edges_df = pd.concat([
            from_this_funded_to_other_funded_events_graph_edges_df, 
            from_other_funded_to_this_funded_events_graph_edges_df
        ])

        other_funded_event_influence_table = dataviz.grant_influence_table(
            grant_id, 
            events_df, 
            other_funded_events_graph_edges_df
        )
        

        # Edges between events funded by this grant and unfunded events
        unfunded_event_ids = list(events_df[
            (events_df['grant_funded_event'] == False)
        ]['event_id'].unique())

        from_this_funded_to_unfunded_events_graph_edges_df = events_graph_edges_df[
            (events_graph_edges_df['from_event_id'].isin(this_grant_funded_event_ids))
            & (events_graph_edges_df['to_event_id'].isin(unfunded_event_ids))
        ].drop(columns='from_event_id').rename(columns={'to_event_id': 'event_id'})
        from_unfunded_to_this_funded_events_graph_edges_df = events_graph_edges_df[
            (events_graph_edges_df['from_event_id'].isin(unfunded_event_ids))
            & (events_graph_edges_df['to_event_id'].isin(this_grant_funded_event_ids))
        ].drop(columns='to_event_id').rename(columns={'from_event_id': 'event_id'})
        unfunded_events_graph_edges_df = pd.concat([
            from_this_funded_to_unfunded_events_graph_edges_df, 
            from_unfunded_to_this_funded_events_graph_edges_df
        ])

        unfunded_event_influence_table = dataviz.grant_influence_table(
            grant_id, 
            events_df, 
            unfunded_events_graph_edges_df
        )

        all_grant_related_events_count = (
            len(filtered_events_df)
            + len(other_funded_event_influence_table)
            + len(unfunded_event_influence_table)
        )

        grant_details['related_events_flag'] = True
        if all_grant_related_events_count == 0:
            grant_details['related_events_flag'] = False
        elif all_grant_related_events_count < 4:
            tables['funded_event_influence_table'] = filtered_events_df.to_dict('records')
            tables['other_funded_event_influence_table'] = other_funded_event_influence_table.to_dict('records')
            tables['unfunded_event_influence_table'] = unfunded_event_influence_table.to_dict('records')
        else:
            charts['grant_influence_chart'] = dataviz.grant_influence_chart(
                filtered_events_df, 
                other_funded_event_influence_table,
                unfunded_event_influence_table
            )
        


              
    params={
        'processing_month': processing_month_display,
        'selection': selection,
        'grant_details': grant_details,
        'charts': charts,
        'tables': tables
    }

    return render_template("grant.html", params=params, form=form)





@app.route('/grants/<grant_id>/grant_related_events', methods=['GET', 'POST'])
def grant_related_events(grant_id):

    processing_month = dataprep.events_df['processing_month'].max()
    processing_month_display = pd.to_datetime(processing_month).strftime('%B %Y')


    
    # event data
    events_df = dataprep.events_df

    events_df['event_start_utc_dt'] = pd.to_datetime(
        events_df['event_start_utc'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d')
    events_df['event_end_utc_dt'] = pd.to_datetime(
        events_df['event_end_utc'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d')

    filtered_events_df = events_df[events_df['grant_id'] == grant_id]

    contributions_df = dataprep.contributions_df
    filtered_contributions_df = contributions_df[contributions_df['grant_id'] == grant_id]

    #filtered_events_graph_edges_df = dataprep.events_graph_edges_df[dataprep.events_graph_edges_df['from_event_id'] == grant_id]

    
    filtered_events_df['participants_count'] = filtered_events_df['participants_count'].fillna(0).astype(int)
    filtered_events_df['grant_country_wikimedia_region'] = filtered_events_df['grant_country_wikimedia_region'].fillna('N/A')


    # filtered_events_df['event_start_utc_dt'] = pd.to_datetime(
    #     filtered_events_df['event_start_utc'].astype('str').str[:8]
    # ).dt.strftime('%Y-%m-%d')
    # filtered_events_df['event_end_utc_dt'] = pd.to_datetime(
    #     filtered_events_df['event_end_utc'].astype('str').str[:8]
    # ).dt.strftime('%Y-%m-%d')

    filtered_events_df['grant_agreement_dt'] = pd.to_datetime(
        filtered_events_df['grant_agreement_dt'].astype('str').str[:8]
    ).dt.strftime('%Y-%m-%d')

    

    # grant details
    grant_details = filtered_events_df[[
        'grant_id', 
        'grant_agreement_dt', 
        'grant_country_list',
        'grant_title',
        'grant_country_list_standardized',
        'grant_country_wikimedia_region',
        'grant_agreement_month',
        'grant_funded_event',
    ]].drop_duplicates().to_dict('records')[0]
    print('TEST =============')
    print(grant_details)

    processing_month = dataprep.events_df['processing_month'].max()
    processing_month_display = pd.to_datetime(processing_month).strftime('%B %Y')

    # Update filter options
    wiki_projects = ['All'] + sorted(list(filtered_contributions_df['wiki_name'].unique()))

    form = forms.EventFilterForm()
    form.wiki_project.choices = wiki_projects

    # apply filters to data
    filtered_events_df, filtered_contributions_df = helpers.apply_filters(
        form, 
        filtered_events_df, 
        filtered_contributions_df,
        mappings
    )

    # selection details and stats
    selection = dict(
        wiki_project = form.wiki_project.data,
        num_edits = filtered_contributions_df['unreverted_edits_count'].sum(),
        num_participants = filtered_events_df['participants_count'].fillna(0).astype(int).sum(),
        num_events = filtered_events_df['event_id'].nunique()
    )



    events_list_cols = [
        'event_id', 
        'event_name', 
        'event_start_utc',  
        'organizers_count', 
        'participants_count'
    ]
    events_list = []
    if len(filtered_events_df) > 0:
        events_list_df = filtered_events_df[events_list_cols].copy()
        events_list_df['event_start_utc'] = pd.to_datetime(
            events_list_df['event_start_utc'].astype('str').str[:8]
        ).dt.strftime('%Y-%m-%d')
        events_list_df['participants_count'] = events_list_df['participants_count'].fillna(0).astype(int)

        events_list = events_list_df.to_dict('records')



    tables = {}


    # Edges between events funded by this grant and events funded by other grants
    this_grant_funded_event_ids = list(filtered_events_df['event_id'].unique())
    other_grant_funded_event_ids = list(events_df[
        (events_df['grant_id'] != grant_id)
        & (events_df['grant_funded_event'] == True)
    ]['event_id'].unique())
    events_graph_edges_df = dataprep.events_graph_edges_df
    from_this_funded_to_other_funded_events_graph_edges_df = events_graph_edges_df[
        (events_graph_edges_df['from_event_id'].isin(this_grant_funded_event_ids))
        & (events_graph_edges_df['to_event_id'].isin(other_grant_funded_event_ids))
    ].drop(columns='from_event_id').rename(columns={'to_event_id': 'event_id'})
    from_other_funded_to_this_funded_events_graph_edges_df = events_graph_edges_df[
        (events_graph_edges_df['from_event_id'].isin(other_grant_funded_event_ids))
        & (events_graph_edges_df['to_event_id'].isin(this_grant_funded_event_ids))
    ].drop(columns='to_event_id').rename(columns={'from_event_id': 'event_id'})
    other_funded_events_graph_edges_df = pd.concat([
        from_this_funded_to_other_funded_events_graph_edges_df, 
        from_other_funded_to_this_funded_events_graph_edges_df
    ])

    other_funded_event_influence_table = dataviz.grant_influence_table(
        grant_id, 
        events_df, 
        other_funded_events_graph_edges_df
    )
    

    # Edges between events funded by this grant and unfunded events
    unfunded_event_ids = list(events_df[
        (events_df['grant_funded_event'] == False)
    ]['event_id'].unique())

    from_this_funded_to_unfunded_events_graph_edges_df = events_graph_edges_df[
        (events_graph_edges_df['from_event_id'].isin(this_grant_funded_event_ids))
        & (events_graph_edges_df['to_event_id'].isin(unfunded_event_ids))
    ].drop(columns='from_event_id').rename(columns={'to_event_id': 'event_id'})
    from_unfunded_to_this_funded_events_graph_edges_df = events_graph_edges_df[
        (events_graph_edges_df['from_event_id'].isin(unfunded_event_ids))
        & (events_graph_edges_df['to_event_id'].isin(this_grant_funded_event_ids))
    ].drop(columns='to_event_id').rename(columns={'from_event_id': 'event_id'})
    unfunded_events_graph_edges_df = pd.concat([
        from_this_funded_to_unfunded_events_graph_edges_df, 
        from_unfunded_to_this_funded_events_graph_edges_df
    ])

    unfunded_event_influence_table = dataviz.grant_influence_table(
        grant_id, 
        events_df, 
        unfunded_events_graph_edges_df
    )

    all_grant_related_events_count = (
        len(filtered_events_df)
        + len(other_funded_event_influence_table)
        + len(unfunded_event_influence_table)
    )


    tables['funded_event_influence_table'] = filtered_events_df.to_dict('records')
    tables['other_funded_event_influence_table'] = other_funded_event_influence_table.to_dict('records')
    tables['unfunded_event_influence_table'] = unfunded_event_influence_table.to_dict('records')
    
    

    print('grant_details:')
    print(grant_details)


                                
    params={
        'processing_month': processing_month_display,
        # 'selection': helpers.selection_details(form, filtered_contributions_df, filtered_events_df),
        #'data_list': events_list,
        'grant_details': grant_details,
        'tables': tables
    }

    return render_template("grant_related_events.html", params=params)










@app.route('/license', methods=['GET'])
def license():
    return render_template("license.html")