from flask_wtf import FlaskForm, CSRFProtect
from wtforms import StringField, SubmitField, SelectField
from wtforms.validators import DataRequired, Length


class FilterForm(FlaskForm):
    wiki_project = SelectField(
        'Wiki project', 
        default="All"
    )
    grant_funding_status = SelectField(
        'Grant funding status', 
        default="All"
    )
    grant_region = SelectField(
        'Grant region', 
        default="All"
    )
    grant_country = SelectField(
        'Grant country', 
        default="All"
    )
    submit = SubmitField('Apply')


class EventFilterForm(FlaskForm):
    wiki_project = SelectField(
        'Wiki project', 
        default="All"
    )
    submit = SubmitField('Apply')


